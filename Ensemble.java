package td3;
import java.lang.*;
import java.util.ArrayList;

public class Ensemble {

	private ArrayList<Integer> elements;
	
	public Ensemble()
	{
		elements = new ArrayList<Integer>();
		
	}
	
	public void ajouter(Integer a)
	{
		elements.add(a);
	}
	
	public int taille()
	{
		return elements.size();
	}
	
	public Integer getElement(int i)
	{
		
		Integer a = new Integer(-1);
		if(i < taille())
		{
			a = elements.get(i);
		}
		else
		{
			System.out.println("Out of bonds");
		}
		return a;
	}
	
	
	public ArrayList<Integer> getElements() {
		return elements;
	}

	public void setElements(ArrayList<Integer> elements) {
		this.elements = elements;
	}

	public String toString_()
	{
		int dim = taille();
		String s = new String();
		s = "";
		if (dim>0)
		{
			System.out.println("Ensemble non vide");
			for(int i=0;i<dim;i++)
			{
				s = s + "/" + getElement(i);
			}
			
		}
		else
		{
			System.out.println("Ensemble vide");
			s = "ensemble vide";
		}
		return s;
	}
	
	public Ensemble union(Ensemble x)
	{
		Ensemble ens = new Ensemble();
		for(int i =0;i<this.taille();i++)
		{
			// Ajout de tout x dans ens
			ens.ajouter(this.getElement(i));
		}
		for(int i =0;i<x.taille();i++)
		{
			if (!(ens.getElements()).contains(x.getElement(i)))
			{
				ens.ajouter(x.getElement(i));
			}
		}
		System.out.println("Fin de l'ajout");
		return ens;
	}
	
	public Ensemble intersection(Ensemble x)
	{
		Ensemble ens = new Ensemble();
		
		int taille1 = this.taille();
		int taille2 = x.taille();
		if(taille1>taille2)
		{
			for(int i =0;i<taille1;i++)
			{
				if (elements.contains(x.getElement(i)))
				{
					ens.ajouter(x.getElement(i));
				}
			}
		}
		else
		{
			for(int i =0;i<taille2;i++)
			{
				if (x.getElements().contains((this.getElement(i))))
				{
					ens.ajouter(this.getElement(i));
				}
			}
		}
		System.out.println("Fin de l'intersection");
		return ens;
	}
}



